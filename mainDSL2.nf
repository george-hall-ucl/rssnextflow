#! /usr/bin/env nextflow

nextflow.enable.dsl = 2

include { umiextract } from './module/umitools'

include { jedup } from './module/jesuite'

include { fastqc } from './module/fastqc'

include { genomeref } from './module/genomeprep'

include { kallistoindex; kallistoquant } from './module/kallisto'

include { staridx; staraligned } from './module/starprocess'

include { subreadCount } from './module/subread.nf'

include { picardinput; picardRNAQC } from './module/picardinput'

include { multiqc } from './module/multiqc.nf'

/////////////////////////////////////
// channel for multqc_configfolder //
/////////////////////////////////////
multiqc_configfolder_chl = Channel.fromPath(params.multiqc_configfolder, checkIfExists: true)

////////////////////////////
// channel for read_pairs //
////////////////////////////
Channel
    .fromFilePairs( "${params.rawdir }/*/${params.readpattern }", size:3) { it.Parent.BaseName }
    .map {
        if (it[0] == 'NM8612_6') {
        it[0] = 'NM8612_A054CD4'
        } else if (it[0] == 'NM8613_7') {
        it[0] = 'NM8613_A054CD8'
    } else if (it[0] == 'NM8614_8') {
        it[0] = 'NM8614_A054CD14'
    } else if (it[0] == 'NM8615_9') {
        it[0] = 'NM8615_A054CD19'
    } else if (it[0] == 'NM8616_10') {
        it[0] = 'NM8616_A054PBMC'
    } else if (it[0] == 'NM8622_17') {
        it[0] = 'NM8622_A033PBMC'
    } else if (it[0] == 'NM8624_19') {
        it[0] = 'NM8624_THC197PBMC'
    } else {
        it[0]
        }
        tuple(it[0], [it[1][0], it[1][1], it[1][2]])
    }
    .ifEmpty { exit 1, "No files match pattern `${params.readpattern}` at path ${params.rawdir}/*/ " }
    .take( params.dev ? params.number_of_inputs : -1)
    .set { read_pairs_ch }


workflow {
    genomeref()

    picardinput(genomeref.out.dnaref, genomeref.out.gtfref)

    umiextract(read_pairs_ch)

    fastqc(umiextract.out.umifastq)

    staridx(genomeref.out.dnaref, genomeref.out.gtfref)

    staraligned(umiextract.out.umifastq, staridx.out.starindex, genomeref.out.gtfref)

    jedup(staraligned.out.bam_sorted)

    kallistoindex(genomeref.out.cdnaref)

    kallistoquant(jedup.out.dedupfq, kallistoindex.out,
               genomeref.out.gtfref,
               picardinput.out.sizesgenome)

    subreadCount(jedup.out.markdupbam.collect(),
             genomeref.out.gtfref)

    picardRNAQC(jedup.out.markdupbam,
             picardinput.out.refFlat,
             picardinput.out.rRNA_list
    )

    multiqc(fastqc.out.collect(),
             kallistoquant.out.kalrun_log.collect(),
             staraligned.out.log_final.collect(),
             subreadCount.out.fcsummary,
            picardRNAQC.out.collect(),
            jedup.out.markdupmetrics.collect(),
            multiqc_configfolder_chl
    )
}
