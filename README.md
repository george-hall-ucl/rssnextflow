## bulkRNAseq with UMI

### Singularity software

Singularity image was built based on **RSSnextflow.def** provided in the repository.  
Built-in software is listed below.

- **FastQC**: [0.11.9](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip)
- **Kallisto**: [0.46.1](https://github.com/pachterlab/kallisto/archive/v0.46.1.tar.gz)
- **STAR** : [2.7.8a](https://github.com/alexdobin/STAR/archive/refs/tags/2.7.8a.tar.gz)
- **featureCount**: [2.0.2](https://sourceforge.net/projects/subread/files/subread-2.0.2/subread-2.0.2-source.tar.gz)
- **PICARD**: [2.25.3-1-g949d7f9-SNAPSHOT](https://github.com/broadinstitute/picard/releases/tag/2.25.1)
- **Drop-seq_tools**: [2.4.0](https://github.com/broadinstitute/Drop-seq/releases/download/v2.4.0/Drop-seq_tools-2.4.0.zip)
- **MultiQC**: [1.10.1,49cafd7](https://multiqc.info/docs/#using-multiqc)
- **UMI-Tools**: [1.1.1](https://umi-tools.readthedocs.io/en/latest/)
- **SAMtools**: [1.12](https://github.com/samtools/samtools/releases/download/1.12/samtools-1.12.tar.bz2)
- **Je**: [2.0.2.RC](https://git.embl.de/grp-gbcs/Je/-/blob/master/dist/je_2.0.RC.tar.gz)


### workflow DAG

![workflow][logo]

[logo]: flowchart_dag.png "FlowChart"


### Quick start

1. install [Nextflow](https://www.nextflow.io/) and add the installation directory to your PATH in .bashrc

```
# it creates a file nextflow in the current dir
curl -s https://get.nextflow.io | bash 

# Run the classic Hello world
./nextflow run hello

# export
export PATH=[installation dir]:$PATH

```
2. A **sampleSheet**, with a header line of **sampleId,read1,read2**, is required. **sampleId** is the sample identifier. **read1,read2** tell nextflow where the FASTQ files are. Absolute path is recommended. For example, a samplesheet with 2 samples looks like below.

```
sampleId,read1,read2
sampleCD4,[FASTQ folder]/sampleCD4_R1_001.fastq.gz,[FASTQ folder]/sampleCD4_R2_001.fastq.gz
sampleCD8,[FASTQ folder]/sampleCD8_R1_001.fastq.gz,[FASTQ folder]/sampleCD8_R2_001.fastq.gz

```

3. Modify process settings in personal.conf, such as executor, to meet your system. When runing the workflow, you need to specify "-c personal.conf". NOTE: Any parameters and config from the command line (-c personal.conf) are prioritised by Nextflow. Resume is set true in *nextflow.config*, which means cached results will be used if present. 

```
# terminal multiplexer
screen

# 2 fastq files/per sample
# UMI incoporated into read headers by BCL Convert

# test run on 2 samples
nextflow run main.nf --dev -c personal.conf --sampleCSV sampleSheet

# run on all samples
nextflow run main.nf -c personal.conf --sampleCSV sampleSheet

```

### Example run

1. Batch1  
    - [Execution report](https://b8307038.gitlab.io/rssnextflow/report.html)
    - [Timeline report](https://b8307038.gitlab.io/rssnextflow/timeline.html)
    - [MultiQC report](https://b8307038.gitlab.io/rssnextflow/multiqc_report.html)
2. Batch2  
    - [Execution report](https://b8307038.gitlab.io/rssnextflow/batch2/report.html)
    - [Timeline report](https://b8307038.gitlab.io/rssnextflow/batch2/timeline.html)
    - [MultiQC report](https://b8307038.gitlab.io/rssnextflow/batch2/multiqc_report.html)
3. [Kallisto vs STAR-featureCounts](https://b8307038.gitlab.io/rssnextflow/feature202compar/kallisto_star_comparison.html ) 

## Credits

- I wrote the first version of the scripts, with codes adapted from https://gist.github.com/ag1805x/55ba0d88f317f63423a4e54adc46eb1f for make_rRNA.sh, [Nextflow](https://www.nextflow.io/docs/latest/index.html), [nf-core/RNAseq](https://github.com/nf-core/rnaseq), and [A tutorial for DLS-2 migration](https://github.com/nextflow-io/nfcamp-tutorial).  
- George Hall (@george-hall-ucl): checking and reviewing the quality of codes
