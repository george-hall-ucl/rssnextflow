#! /usr/bin/env nextflow

nextflow.enable.dsl = 2

include { jedupnew } from './module/jesuite'

include { fastqc } from './module/fastqc'

include { genomeref } from './module/genomeprep'

include { kallistoindex; kallistoquant } from './module/kallisto'

include { staridx; staraligned } from './module/starprocess'

include { subreadCount } from './module/subread.nf'

include { picardinput; picardRNAQC } from './module/picardinput'

include { multiqc } from './module/multiqc.nf'

/////////////////////////////////////
// channel for multqc_configfolder //
/////////////////////////////////////
multiqc_configfolder_chl = Channel.fromPath(params.multiqc_configfolder, checkIfExists: true)

////////////////////////////
// channel for read_pairs //
////////////////////////////

if (params.sampleCSV) {
    sampleFile = file(params.sampleCSV, checkIfExists: true)
} else {
    exit 1, 'samplecsv (--sampleCSV) is required!'
}

Channel
    .fromPath( sampleFile )
    .splitCsv( header:true )
    .map{ row -> tuple( row.sampleId, [ file( row.read1, checkIfExists: true ), file( row.read2, checkIfExists: true )] )}
    .take( params.dev ? params.number_of_inputs : -1)
    .set { read_pairs_ch }


workflow {
    genomeref()

    picardinput(genomeref.out.dnaref, genomeref.out.gtfref)

    fastqc(read_pairs_ch)

    staridx(genomeref.out.dnaref, genomeref.out.gtfref)

    staraligned(read_pairs_ch, staridx.out.starindex, genomeref.out.gtfref)

    jedupnew(staraligned.out.bam_sorted)

    kallistoindex(genomeref.out.cdnaref)

    kallistoquant(jedupnew.out.dedupfq, kallistoindex.out,
               genomeref.out.gtfref,
               picardinput.out.sizesgenome)

    subreadCount(jedupnew.out.markdupbam.collect(),
             genomeref.out.gtfref)

    picardRNAQC(jedupnew.out.markdupbam,
             picardinput.out.refFlat,
             picardinput.out.rRNA_list
    )

    multiqc(fastqc.out.collect(),
             kallistoquant.out.kalrun_log.collect(),
             staraligned.out.log_final.collect(),
             subreadCount.out.fcsummary,
            picardRNAQC.out.collect(),
            jedupnew.out.markdupmetrics.collect(),
            multiqc_configfolder_chl
    )
}
