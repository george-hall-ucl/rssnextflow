
process kallistoindex {
    label 'process_himem'
    publishDir 'software/kallisto'

    executor = 'local'

    input:
    path cdnaref

    output:
    path 'Homo_sapiens.GRCh38.kallisto.idx', emit: kalindex

    script:
    real_cdnagz = "readlink ${cdnaref}"

    """

    kallisto index -i Homo_sapiens.GRCh38.kallisto.idx \$(${real_cdnagz})

    """
}

process kallistoquant {
    tag "$sampleId"
    label 'process_medium'
    publishDir "${params.outdir}/kallisto/${sampleId}"

    input:
    tuple val(sampleId), path(reads)
    path kalindex
    path gtfref
    path sizesgenome

    output:
    path 'abundance.h5', emit: kalrun_h5
    path 'abundance.tsv', emit: kalrun_abundance
    path '*pseudoalignments.bam', emit: kalbam
    path '*pseudoalignments.bam.bai', emit: kalbai
    path 'run_info.json', emit: kalrun_json
    path "${kalrun}",    emit: kalrun_log

    script:

    kalrun = "${sampleId}_run.log"

    """

    kallisto quant -i ${kalindex} \\
    -o ./ -b 100 --fr-stranded \\
    --threads=${task.cpus} --genomebam --gtf ${gtfref} \\
    --chromosomes ${sizesgenome} \\
    ${reads[0]} ${reads[1]}

    mv pseudoalignments.bam ${sampleId}.pseudoalignments.bam
    mv pseudoalignments.bam.bai ${sampleId}.pseudoalignments.bam.bai
    cp .command.log ${kalrun}
    """
}
