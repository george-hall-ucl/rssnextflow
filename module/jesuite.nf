
// umi sep=_
process jedup {
    tag "${bamfiles.simpleName}"
    label 'process_himemje'
    publishDir "${params.outdir}/jedup/${bamfiles.simpleName}"

    input:
    path bamfiles

    output:
    path("*.markdup.bam"), emit: markdupbam
    path("*.markdup.metrics"), emit: markdupmetrics
    tuple val("${bamfiles.simpleName}"), path('*.fastq.gz'), emit: dedupfq

    script:
    sampleId = "${bamfiles.simpleName}"
    //def maxreads= 180000 * task.attempt
    memgb = 12 * task.attempt

    """
    java -Xmx${memgb}G -jar \$JE202 markdupes INPUT=$bamfiles \\
    OUTPUT=${sampleId}.markdup.bam \\
    METRICS_FILE=${sampleId}.markdup.metrics \\
    MISMATCHES=1 \\
    SPLIT_CHAR=_ SLOTS=-1 \\
    ASSUME_SORT_ORDER=coordinate \\
    REMOVE_DUPLICATES=false \\
    DUPLICATE_SCORING_STRATEGY=SUM_OF_BASE_QUALITIES

    samtools view -F 0x400 -b -@ $task.cpus ${sampleId}.markdup.bam | \\
    samtools sort -n -@ $task.cpus - | \\
    samtools fastq -@ $task.cpus - \\
    -1 ${sampleId}dedup_R1.fastq.gz -2 ${sampleId}dedup_R3.fastq.gz -n

    """
}

// umi sep=:, new format
process jedupnew {
    tag "${bamfiles.simpleName}"
    label 'process_himemje'
    publishDir "${params.outdir}/jedup/${bamfiles.simpleName}"

    input:
    path bamfiles

    output:
    path("*.markdup.bam"), emit: markdupbam
    path("*.markdup.metrics"), emit: markdupmetrics
    tuple val("${bamfiles.simpleName}"), path('*.fastq.gz'), emit: dedupfq

    script:
    sampleId = "${bamfiles.simpleName}"
    //def maxreads= 180000 * task.attempt
    memgb = 12 * task.attempt

    """
    java -Xmx${memgb}G -jar \$JE202 markdupes INPUT=$bamfiles \\
    OUTPUT=${sampleId}.markdup.bam \\
    METRICS_FILE=${sampleId}.markdup.metrics \\
    MISMATCHES=1 \\
    SPLIT_CHAR=: SLOTS=-1 \\
    ASSUME_SORT_ORDER=coordinate \\
    REMOVE_DUPLICATES=false \\
    DUPLICATE_SCORING_STRATEGY=SUM_OF_BASE_QUALITIES

    samtools view -F 0x400 -b -@ $task.cpus ${sampleId}.markdup.bam | \\
    samtools sort -n -@ $task.cpus - | \\
    samtools fastq -@ $task.cpus - \\
    -1 ${sampleId}dedup_R1.fastq.gz -2 ${sampleId}dedup_R3.fastq.gz -n

    """
}
