
process staridx {
    label 'process_8himem'
    publishDir 'software/STAR'

    input:
    path dnaref
    path gtfref

    output:
    path 'star', emit: starindex

    script:

    realdnagz = "readlink ${dnaref}"
    realgtfgz = "readlink ${gtfref}"

    """
    zcat \$(${realdnagz}) > ${dnaref.baseName}
    zcat \$(${realgtfgz}) > ${gtfref.baseName}

    mkdir star
    STAR \\
    --runMode genomeGenerate \\
    --genomeDir star/ \\
    --genomeFastaFiles ${dnaref.baseName} \\
    --sjdbGTFfile  ${gtfref.baseName} \\
    --sjdbOverhang 99 \\
    --runThreadN $task.cpus

    """
}

process staraligned {
    tag "$sampleId"
    label 'process_8himem'
    publishDir "${params.outdir}/STAR/${sampleId}"

    input:
    tuple val(sampleId), path(reads)
    path starindex
    path gtfref

    output:
    path("*.sortedByCoord.out.bam")  , emit: bam_sorted
    path("*.Log.final.out")   , emit: log_final
    path("*.Log.out")         , emit: log_out
    path("*.Log.progress.out"), emit: log_progress

    path("*.Aligned.out.bam")        , optional:true, emit: bam
    path("*.tab")                    , optional:true, emit: tab

    script:

    realgtfgz = "readlink ${gtfref}"

    """

    zcat \$(${realgtfgz}) > ${gtfref.baseName}

    STAR --runThreadN $task.cpus \\
    --genomeDir ${starindex} \\
    --sjdbGTFfile  ${gtfref.baseName} \\
    --sjdbOverhang 99 \\
    --readFilesIn ${reads[0]} ${reads[1]} \\
    --readFilesCommand zcat \\
    --outSAMtype BAM SortedByCoordinate \\
    --twopassMode Basic \\
    --outFileNamePrefix ${sampleId + '.'}

    rm ${gtfref.baseName}

    """
}
