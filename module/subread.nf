
// reads counts

process subreadCount {
    label 'process_medium'
    publishDir "${params.outdir}/featureCounts"

    input:
    path bamfiles
    path gtfref

    output:
    path "${params.fctable}", emit: counts
    path "${fc_summary}", emit: fcsummary

    script:
    realgtfgz = "readlink ${gtfref}"
    fc_summary = "${params.fctable}.summary"
    fctmp = "${params.fctable}.tmp"

    """
    zcat \$(${realgtfgz}) > ${gtfref.baseName}

    # -p for paired-ended
    # -s strand
    # -B requireBothEndsMapped
    # -C chimeric fragments will NOT be counted
    # -M Multiplemapping reads
    
    # --countReadPairs, v2.02 new arguments
    # otherwise, counts as indiv reads, twice as before

    featureCounts -T $task.cpus -C -B -p -s 1 -M \\
    -a ${gtfref.baseName} -t exon -g gene_id \\
    --extraAttributes gene_name,gene_biotype \\
    --ignoreDup --countReadPairs \\
    -o "${params.fctable}" ${bamfiles}

    mv ${fc_summary} ${fctmp}

    grep -v ^Unassigned_Duplicate ${fctmp} > ${fc_summary}

    rm -f ${gtfref.baseName}
    """
}

