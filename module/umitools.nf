
process umiextract {
    tag "${sampleId}"
    label 'process_1low'
    publishDir "${params.outdir}/umifastq/${sampleId}"

    input:
    tuple val(sampleId), path(reads)

    output:
    tuple val(sampleId), path('*umi.fastq.gz'), emit: umifastq
    tuple val(sampleId), path('*.log')        , emit: log

    script:
    (foo0, foo1, foo2, foo3, foo4) = ("${reads[1]}" =~ /(.+)_(.+)_(R\d{1})_(\d+\.fastq\.gz)/)[0]
    if ( "${foo3}" != 'R2') { exit 1, 'checking the order of reads' }

    """
    umi_tools extract --bc-pattern=NNNNN --stdin=${reads[1]} \\
    --read2-in=${reads[0]} --stdout="${sampleId}_R1.umi.fastq.gz" \\
    --read2-stdout > "${sampleId}_R1.log"

    umi_tools extract --bc-pattern=NNNNN --stdin=${reads[1]} \\
    --read2-in=${reads[2]} --stdout="${sampleId}_R3.umi.fastq.gz" \\
    --read2-stdout > "${sampleId}_R3.log"
    """
}

