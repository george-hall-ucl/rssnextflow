
process multiqc {
    publishDir "${params.outdir}", mode: 'copy'

    executor = 'local'

    //beforeScript 'source /home/wyl37/mypython3base/bin/activate'

    input:
    path fastqc
    path kallistoquant
    path staraligned
    path subreadCount
    path picard
    path jemarkdup
    path multiqc_configfolder

    output:
    path 'multiqc_report.html'
    path 'multiqc_data', emit: multiqcdata

    script:
    """
    cp $multiqc_configfolder/* .

    echo "custom_logo: \$PWD/clusterlogo.png" >> multiqc_config.yaml

    multiqc -v .
    """
}
